<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Illuminate\Support\Str;
use Faker\Generator as Faker;
use App\dashboard\Users\Models\Users;

$factory->define(Users::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('password'),
        'firstName' => $faker->firstName,
        'lastName' => $faker->lastName,
        'displayName' => $faker->name,
        'userName' => $faker->userName,
        'avatar' => $faker->imageUrl(),
        'remember_token' => Str::random(10),
    ];
});
