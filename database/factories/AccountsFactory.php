<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\dashboard\Accounts\Models\Accounts;

$factory->define(Accounts::class, function (Faker $faker) {
    return [
        'userId' => function () {
            return factory(\App\dashboard\Users\Models\Users::class)->create()->id;
        },
        'name' => $faker->name,
        'url' => $faker->url,
        'username' => $faker->userName,
        'password' => $faker->password,
        'tag' => $faker->name,
    ];
});
