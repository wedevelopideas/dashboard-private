<?php

namespace App\dashboard\Accounts\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Accounts.
 *
 * @property int $id
 * @property int $userId
 * @property string $name
 * @property string $url
 * @property string $username
 * @property string $password
 * @property string $tag
 */
class Accounts extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userId',
        'name',
        'url',
        'username',
        'password',
        'tag',
    ];
}
