<?php

namespace App\dashboard\Accounts\Repositories;

use App\dashboard\Accounts\Models\Accounts;

class AccountsRepository
{
    /**
     * @var Accounts
     */
    private $accounts;

    /**
     * AccountsRepository constructor.
     * @param Accounts $accounts
     */
    public function __construct(Accounts $accounts)
    {
        $this->accounts = $accounts;
    }

    /**
     * Number of all passwords of the user.
     *
     * @param int $userId
     * @return mixed
     */
    public function countAllPasswords(int $userId)
    {
        return $this->accounts
            ->where('userId', $userId)
            ->count();
    }

    /**
     * Get all passwords of the user.
     *
     * @param int $userId
     * @return mixed
     */
    public function getAllPasswords(int $userId)
    {
        return $this->accounts
            ->where('userId', $userId)
            ->orderBy('name', 'ASC')
            ->get();
    }
}
