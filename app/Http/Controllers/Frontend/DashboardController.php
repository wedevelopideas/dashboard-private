<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\dashboard\Accounts\Repositories\AccountsRepository;

class DashboardController extends Controller
{
    /**
     * @var AccountsRepository
     */
    private $accountsRepository;

    /**
     * DashboardController constructor.
     * @param AccountsRepository $accountsRepository
     */
    public function __construct(AccountsRepository $accountsRepository)
    {
        $this->accountsRepository = $accountsRepository;
    }

    /**
     * Shows the dashboard of the user.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dashboard()
    {
        $userId = auth()->id();

        $totalPasswords = $this->accountsRepository->countAllPasswords($userId);
        $passwords = $this->accountsRepository->getAllPasswords($userId);

        return view('dashboard')
            ->with('passwords', $passwords)
            ->with('totalPasswords', $totalPasswords);
    }
}
