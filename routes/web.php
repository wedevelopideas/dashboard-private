<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Frontend'], function () {
    Route::group(['namespace' => 'Auth'], function () {
        Route::get('', ['uses' => 'LoginController@showForm', 'as' => 'index']);
        Route::get('login', ['uses' => 'LoginController@showForm', 'as' => 'login']);
        Route::post('login', ['uses' => 'LoginController@login']);
        Route::post('logout', ['uses' => 'LoginController@logout', 'as' => 'logout']);
    });

    Route::group(['middleware' => 'auth'], function () {
        Route::get('dashboard', ['uses' => 'DashboardController@dashboard', 'as' => 'dashboard']);
    });
});
