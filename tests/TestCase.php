<?php

namespace Tests;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    /**
     * Set up the test.
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * Reset the migrations.
     */
    protected function tearDown(): void
    {
        parent::tearDown();
    }
}
