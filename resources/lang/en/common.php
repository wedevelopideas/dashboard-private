<?php

return [
    'email' => 'Email',
    'errors' => [
        'login_failed' => 'Login failed',
    ],
    'login' => 'Login',
    'password' => 'Password',
    'statistics' => 'Statistics',
    'total_passwords' => 'Total passwords',
];
