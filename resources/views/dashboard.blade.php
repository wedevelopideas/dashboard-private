@extends('layouts.frontend')
@section('title', 'dashboard')
@section('content')
    <div class="ui stackable grid">
        <div class="three wide column">
            @include('_partials.dashboard.user')
        </div>
        <div class="three wide column">
            @include('_partials.dashboard.statistics')
        </div>
        <div class="sixteen wide column">
            @include('_partials.dashboard.passwords')
        </div>
    </div>
@endsection