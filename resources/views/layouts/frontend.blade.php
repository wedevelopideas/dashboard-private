<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>dashboard | private</title>
    <meta name="description" content="private dashboard">
    <link rel="stylesheet" href="{{ asset('assets/semantic.min.css') }}">
    <style type="text/css">
        body {
            background-color: #000000;
        }
        .main.container {
            margin-top: 7em;
        }
    </style>
</head>
<body>
<div class="ui fixed inverted menu">
    <div class="ui fluid container">
        <a href="{{ route('dashboard') }}" class="header item">
            dashboard | private
        </a>
        <div class="right menu">
            <div class="item">
                {{ auth()->user()->firstName }}
            </div>
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" title="{{ trans('common.logout') }}" class="icon item">
                <i class="sign out icon"></i>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf

            </form>
        </div>
    </div>
</div>
<div class="ui main fluid container">
@yield('content')
</div>
<script src="{{ asset('assets/jquery.min.js') }}"></script>
<script src="{{ asset('assets/semantic.min.js') }}"></script>
</body>
</html>