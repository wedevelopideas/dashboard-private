@if (Session::has('error'))
    <div class="ui negative message">
        <div class="header">
            {{ Session::get('error') }}
        </div>
    </div>
@endif