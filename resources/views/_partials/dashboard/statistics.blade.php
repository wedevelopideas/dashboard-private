<div class="ui inverted segment">
    <h3 class="ui center aligned header">
        {{ trans('common.statistics') }}
    </h3>
    <div class="ui inverted list">
        <div class="item">
            <div class="content">
                <div class="header">
                    {{ trans('common.total_passwords') }}
                </div>
                <div class="description">
                    {{ $totalPasswords }}
                </div>
            </div>
        </div>
    </div>
</div>