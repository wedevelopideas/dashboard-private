<div class="ui inverted segment">
    <div class="ui inverted divided list">
        @foreach($passwords as $password)
            <div class="item">
                <div class="content">
                    <div class="header">{{ $password->name }}</div>
                    <div class="description">{{ $password->username }}</div>
                </div>
            </div>
        @endforeach
    </div>
</div>