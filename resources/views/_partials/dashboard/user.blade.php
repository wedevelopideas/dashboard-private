<div class="ui inverted center aligned segment">
    <div class="ui circular small image">
        <img src="{{ auth()->user()->avatar }}" alt="{{ auth()->user()->displayName }}">
    </div>
    <h3 class="ui header">
        {{ auth()->user()->displayName }}
    </h3>
</div>